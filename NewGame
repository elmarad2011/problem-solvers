
import java.awt.BasicStroke;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.DeviationRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.data.xy.YIntervalSeries;
import org.jfree.data.xy.YIntervalSeriesCollection;
import org.jfree.ui.RectangleInsets;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class NewGame {

	private ArrayList<SolverAgent> solvers;
	private ArrayList<ProposerAgent> proposers;
	private ArrayList<Problem> problems;
	private String typeOfMatching;
	private ArrayList<Round> rounds;
	private ArrayList<Double> solverAvg;
	private ArrayList<Double> solverDevMAvg;			// stores the minus deviation of the agents experience
	private ArrayList<Double> solverDevPAvg;			// stores the plus deviation of the agents experience	
	private ArrayList<Double> problemAvg;
	private ArrayList<Double> problemMDevMAvg;			//	stores the minus deviation of the average problem difficulty
	private ArrayList<Double> problemPDevPAvg;			//	stores the plus deviation of the average problem difficulty	
	private ArrayList<Double> stdDevSolvers;
	private ArrayList<Double> stdDevProblems;
	private WritableWorkbook workbook;
	private WritableSheet problemsSheet;
	private WritableSheet agentsSheet;
	private WritableSheet averagesSheet;
	private WritableSheet detailsSheet;
	private WritableSheet compositeSheet;
	private Label label;
	int t = 1;
	public static int roundsCount;
	public static int round_no;
	public static boolean Eradication;
	String path;
	int row = 0;
	int row2 = 0;
	static String data;
	double avgAgentsSolvedperGame = 0.0;
	Double[] avgAgentsSolvedperRound;
	int[] agentsPerRound = new int[roundsCount + 1];
	int[] proposersPerRound = new int[roundsCount + 1];

	// Constructor for Game
	public NewGame(int roundsCount, ArrayList<SolverAgent> solvers,
			ArrayList<ProposerAgent> proposers, String typeOfMatching) {

		// Constructs a game using an arraylist of solvers, an arraylist of
		// proposers, the type of matching, an arraylist of rounds
		this.solvers = solvers;
		this.proposers = proposers;
		this.typeOfMatching = typeOfMatching;
		this.rounds = new ArrayList<Round>();
		this.roundsCount = roundsCount;
		this.solverAvg = new ArrayList<Double>();
		this.solverDevMAvg = new ArrayList<Double>();
		this.solverDevPAvg = new ArrayList<Double>();
		this.problemAvg = new ArrayList<Double>();
		this.problemMDevMAvg = new ArrayList<Double>();
		this.problemPDevPAvg = new ArrayList<Double>();
		this.stdDevProblems = new ArrayList<Double>();
		this.stdDevSolvers = new ArrayList<Double>();

		// Gets the path to Desktop
		path = System.getProperty("user.home");
		path = path.replace("/", "\\");

		// Gets current date to use when creating the excel sheets and pictures
		// on the desktop
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy-HHmmss");
		data = dateFormat.format(date);

		// creates the excel file
		try {
			workbook = Workbook.createWorkbook(new File(path
					+ "\\Desktop\\simulation" + data + ".xls"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		// adds the sheets to the excel file
		problemsSheet = workbook.createSheet("ProblemsSheet", 0);
		agentsSheet = workbook.createSheet("AgentsSheet", 1);
		averagesSheet = workbook.createSheet("AveragesSheet", 2);
		detailsSheet = workbook.createSheet("DetailsSheet", 3);
		compositeSheet = workbook.createSheet("CompositeSheet", 3);

		try {
			// headers for the tables in the excel file
			writeStringToCell(problemsSheet, 0, 0, "Rounds ");
			writeStringToCell(problemsSheet, 1, 0, "Problems ");
			writeStringToCell(problemsSheet, 2, 0, "Type ");
			writeStringToCell(problemsSheet, 3, 0, "Difficulty ");
			writeStringToCell(problemsSheet, 4, 0, "Reward ");
			writeStringToCell(problemsSheet, 5, 0, "Solved ");

			// puts the information about the solver agents in the excel file
			// before they have solved anything
			// in order to see their initial state before they started the game
			outputSolvers(solvers);

		} catch (WriteException e) {
			e.printStackTrace();
		}
	}

	// puts the information about the solver agents in the excel file before
	// they have solved anything
	// in order to see their initial state before they started the game
	public void outputSolvers(ArrayList<SolverAgent> solvers)
			throws RowsExceededException {

		// puts the headers for the agents table
		writeStringToCell(agentsSheet, 0, 0, "Round");
		writeStringToCell(agentsSheet, 1, 0, "Agent");
		writeStringToCell(agentsSheet, 2, 0, "Skills");
		writeStringToCell(agentsSheet, 3, 0, "Exprience");
		writeStringToCell(agentsSheet, 4, 0, "Composite Exp");
		writeStringToCell(agentsSheet, 5, 0, "Money");
		writeStringToCell(agentsSheet, 6, 0, "Has solved");
		writeStringToCell(agentsSheet, 7, 0, "Was in group");
		writeStringToCell(agentsSheet, 8, 0, "Has rejected");

		Collections.sort(solvers);

		// go through the agents and write their information in the excel
		// file
		for (int i = 0; i < solvers.size(); i++) {
			//System.out.println(solvers.get(i).getExp(0)+"-"+ solvers.get(i).getExp(1)+"-"+ solvers.get(i).getExp(2)+"-"+ solvers.get(i).getExp(3));
			writeStringToCell(agentsSheet, 1, t, solvers.get(i).toString());
			writeStringToCell(agentsSheet, 2, t, solvers.get(i).getSkillsString());
			writeStringToCell(agentsSheet, 3, t, solvers.get(i).getExperienceString());
			writeStringToCell(agentsSheet, 4, t, solvers.get(i).compositeExpString());
			writeNumberToCell(agentsSheet, 5, t, solvers.get(i).getMoney());
			//writeNumberToCell(agentsSheet, 4, t, solvers.get(i).getExp(0));
			//writeNumberToCell(agentsSheet, 4, t, solvers.get(i).getExp(1));
			//writeNumberToCell(agentsSheet, 4, t, solvers.get(i).getExp(2));
			//writeNumberToCell(agentsSheet, 4, t, solvers.get(i).getExp(3));
			t++;
		}
	}

	// method to run the game
	// runs the rounds using the type of matching chosen
	public void runGame() throws RowsExceededException, WriteException {

		writeStringToCell(detailsSheet, 1, 1, "Initial number of solver agents ");
		writeNumberToCell(detailsSheet, 2, 1, solvers.size());
		writeStringToCell(detailsSheet, 1, 2, "Initial number of problem agents ");
		writeNumberToCell(detailsSheet, 2, 2, proposers.size());
		writeStringToCell(detailsSheet, 1, 3,  "Max experience at start ");
		writeNumberToCell(detailsSheet, 2, 3,  Consts.MAX_EXPERIENCE_AT_START);
		writeStringToCell(detailsSheet, 1, 4,  "Number of rounds ");
		writeNumberToCell(detailsSheet, 2, 4,  Consts.ROUNDS_NUMBER);
		writeStringToCell(detailsSheet, 1, 5,  "Type of game ");
		writeStringToCell(detailsSheet, 2, 5,  typeOfMatching.toString());
		writeStringToCell(detailsSheet, 1, 6,  "Deadline ");
		writeStringToCell(detailsSheet, 2, 6,  "" + Consts.DEADLINE);
		writeStringToCell(detailsSheet, 1, 7,  "Chance of solver generation ");
		writeNumberToCell(detailsSheet, 2, 7,  Consts.CHANCE_OF_SOLVER_GENERATION);
		writeStringToCell(detailsSheet, 1, 8,  "Chance of proposer generation ");
		writeNumberToCell(detailsSheet, 2, 8,  Consts.CHANCE_OF_PROPOSER_GENERATION);
		writeStringToCell(detailsSheet, 1, 9,  "Removal threshold for solver agents");
		writeNumberToCell(detailsSheet, 2, 9,  Consts.REMOVAL_SOLVER_TRESHOLD);
		writeStringToCell(detailsSheet, 1, 10, "Removal threshold for proposer agents");
		writeNumberToCell(detailsSheet, 2, 10,  Consts.REMOVAL_PROPOSER_TRESHOLD);

		agentsPerRound = new int[roundsCount + 1];
		proposersPerRound = new int[roundsCount + 1];

		// create and run first round
		Round round = new Round(solvers, proposers);
		System.out.println("1st Round, proposer size: )" + round.getProposers().size());
		System.out.println("1st Round, solver size: )" + round.getSolvers().size());
		agentsPerRound[0] = round.getSolvers().size();
		proposersPerRound[0] = round.getProposers().size();
		round.runRound(typeOfMatching, 1);
		agentsPerRound[1] = round.getSolvers().size();
		proposersPerRound[1] = round.getProposers().size();

		double sum = 0;
		for (int i = 0; i < round.getSolvers().size(); i++) {
			if (round.getSolvers().get(i).hasSolved() != null)
				sum++;
		}

		avgAgentsSolvedperRound = new Double[roundsCount];
		avgAgentsSolvedperRound[0] = sum / round.getSolvers().size();

		// output first round to the excel sheet
		outputRound(round, 1);
		outputComposite(round, 0);
		this.rounds.add(round);
		round_no = 0;
		Eradication = false;
		// output the rest of the rounds to the excel file
		for (int i = 0; i < roundsCount - 1; i++) {
			// create round
			round_no ++;
			sum = 0;
			round = new Round(this.rounds.get(i).getSolvers(), this.rounds.get(
					i).getProposers(), this.rounds.get(i).getProblems());
			// run round
			System.out.println("Round No: " + i +", solver size: " + round.getSolvers().size() + ", proposers size: " + round.getProposers().size());
			round.runRound(typeOfMatching, i + 2);
			agentsPerRound[i + 2] = round.getSolvers().size();
			proposersPerRound[i + 2] = round.getProposers().size();
			for (int j = 0; j < round.getSolvers().size(); j++) {
				if (round.getSolvers().get(j).hasSolved() != null)
					sum++;
			}

			avgAgentsSolvedperRound[i + 1] = sum / round.getSolvers().size();

			// output round to excel
			outputRound(round, i + 2);
			outputComposite(round,i+1);
			this.rounds.add(round);
			// output round to experience, problems arrays
			double av;
			av = Double.parseDouble(avgExpRound(round));
			solverAvg.add(i, av);
			solverDevMAvg.add(i,(av -  (Double.parseDouble(stdDevSolvers(round)))));
			solverDevPAvg.add(i,(av +  (Double.parseDouble(stdDevSolvers(round)))));
			av = Double.parseDouble(avgProblemRound(round));
			problemAvg.add(i, av);
			problemMDevMAvg.add(i,(av -  (Double.parseDouble(stdDevProblems(round)))));
			problemPDevPAvg.add(i,(av + (Double.parseDouble(stdDevProblems(round)))));
			
		}

		// output averages in the excel file and graph images
		outputAverages();

		try {
			// write to excel file
			workbook.write();
			// save excel file and close it
			workbook.close();
		} catch (IOException | WriteException e) {
			e.printStackTrace();
		}

	}
	
	public String avgExpRound(Round round)
	{
		double counter = 0;
		double average , variance, standard_variation;
		variance = 0;
		standard_variation = 0;
		for (int i = 0; i < round.getSolvers().size(); i++) {
			// calculate the solver avg of the round
			counter = counter + Double.parseDouble(round.getSolvers().get(i).compositeExpString());
		}
		String avg = "" + ((double)counter / (double)round.getSolvers().size());
		/*average = ((double)counter / (double)round.getSolvers().size());
		System.out.println("Average agent experience is: " + average);
		// calculate the variance
		for (int i = 0; i < round.getSolvers().size(); i++) {
            variance += (Double.parseDouble(round.getSolvers().get(i).compositeExpString()) - average) * (Double.parseDouble(round.getSolvers().get(i).compositeExpString()) - average);
		}
		variance = variance / round.getSolvers().size();
		standard_variation = Math.sqrt(variance);
		System.out.println("Standard variation equals: " + standard_variation);
		solverAvg.add((double)counter / (double)round.getSolvers().size());
		solverDevMAvg.add(average - standard_variation);
		System.out.println("Standard minus deviation equals: " + (average - standard_variation));
		solverDevPAvg.add(average + standard_variation);
		System.out.println("Standard plus deviation equals: " + (average + standard_variation));*/
		System.out.println("Average agent experience is: " + avg);
		return avg;
	}
	
	public String stdDevSolvers(Round round)
	{
		double counter = 0;
		double sd = 0;
		System.out.println("Solvers size equals: " + round.getSolvers().size());
		/*if (round.getSolvers().size() == 0)
		{
		    final JPanel panel = new JPanel();
		    JOptionPane.showMessageDialog(panel, "ELE - Extincion Level Event, Solvers have been eradicated!" , "Error", JOptionPane.ERROR_MESSAGE);
		    System.exit(0);
		}		*/
		for (int i = 0; i < round.getSolvers().size(); i++) {
			// calculate the solver avg of the round
			counter = counter + Double.parseDouble(round.getSolvers().get(i).compositeExpString());
			//System.out.println("counter: " + counter);
		}
		double avg = (double)counter / (double)round.getSolvers().size();
		for (int i = 0; i < round.getSolvers().size(); i++) {
			// calculate standard deviation
			sd = sd + Math.pow(Double.parseDouble(round.getSolvers().get(i).compositeExpString()) - avg, 2);
			//System.out.println("Sd equals: " + sd);
		}
		sd = sd / round.getSolvers().size();
		sd = Math.sqrt(sd);
		
		//System.out.println("Sd equals: " + sd);
		//double retSD = round(sd, 3);
		stdDevSolvers.add(sd);
		System.out.println("Solvers deviation for round: " + round + ", is: " + sd);
		return "" + sd;
	}
	
	public String stdDevProblems(Round round)
	{
		double counter = 0;
		double sd = 0;
		for (int i = 0; i < round.getProblems().size(); i++) {
			// calculate the solver avg of the round
			counter = counter + Double.parseDouble(round.getProblems().get(i).compositeProblemString());
		}
		double avg = (double)counter / (double)round.getProblems().size();
		for (int i = 0; i < round.getProblems().size(); i++) {
			// calculate the solver avg of the round
			sd = sd + Math.pow(Double.parseDouble(round.getProblems().get(i).compositeProblemString()) - avg, 2);
		}
		sd = sd / round.getProblems().size();
		sd = Math.sqrt(sd);
		//double retSD = round(sd, 3);
		System.out.println("Provlems deviation for round: " + round + ", is: " + sd);
		stdDevProblems.add(sd);
		return "" + sd;
	}
	
	@SuppressWarnings("finally")
	public static double round(double value, int places) {
		boolean error = false;
	    if (places < 0) throw new IllegalArgumentException();
	    try
	    {
	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    }
	    catch (java.lang.NumberFormatException NE)
	    {
	    	System.out.println("Number format exception: " + NE.getMessage());
	    	error = true;
	    }
	    finally
	    {
	    	if (error)
	    	{
	    		return 0;
	    	}
	    	else
	    	{	   
	    		BigDecimal bd = new BigDecimal(value);
	    	    return bd.doubleValue();
	    	}
	    }    
	}
	
	public String avgProblemRound(Round round)
		{
		double counter = 0;
		double average, variance, standard_deviation;
		average = 0;
		variance = 0;
		standard_deviation = 0;		
		for (int i = 0; i < round.getProblems().size(); i++) {
			// calculate the problem avg of the round
			counter = counter + Double.parseDouble(round.getProblems().get(i).compositeProblemString());
		}
		average = ((double)counter / (double)round.getProblems().size());
		System.out.println("Average problem diff is: " + average);
		String avg = String.valueOf(average);
		// calculate the variance
		/*for (int i = 0; i < round.getProblems().size(); i++) {
            variance += (Double.parseDouble(round.getProblems().get(i).compositeProblemString()) - average) * (Double.parseDouble(round.getProblems().get(i).compositeProblemString()) - average);
		}
		variance = variance / round.getSolvers().size();
		standard_deviation = Math.sqrt(variance);
		System.out.println("Diff Standard variation equals: " + standard_deviation);
		problemAvg.add((double)counter / (double)round.getSolvers().size());
		System.out.println("Diff minus deviation equals : " + (average - standard_deviation));
		problemMDevMAvg.add(average - standard_deviation);
		System.out.println("Diff plus deviation equals : " + (average + standard_deviation));
		problemPDevPAvg.add(average + standard_deviation);*/
		return avg;
	}
	
	public void outputComposite(Round round, int i) {
		writeStringToCell(compositeSheet, 0, 0, "Round");
		writeStringToCell(compositeSheet, 1, 0, "Agent");
		writeStringToCell(compositeSheet, 2, 0, "Agent stdDev");
		writeStringToCell(compositeSheet, 3, 0, "Problems");
		writeStringToCell(compositeSheet, 4, 0, "Problems stdDev");
		writeStringToCell(compositeSheet, 5, 0, "Problem Amount");
		writeStringToCell(compositeSheet, 6, 0, "Solver Amount");
		writeNumberToCell(compositeSheet, 0, i+1, i);
		writeStringToCell(compositeSheet, 1, i+1, avgExpRound(round));
		writeStringToCell(compositeSheet, 2, i+1, stdDevSolvers(round));
		writeStringToCell(compositeSheet, 3, i+1, avgProblemRound(round));
		writeStringToCell(compositeSheet, 4, i+1, stdDevProblems(round));
		writeStringToCell(compositeSheet, 5, i+1, String.valueOf(round.getProblems().size()));
		writeStringToCell(compositeSheet, 6, i+2, String.valueOf(round.getSolvers().size()));
	}

	// method to output round in excel
	public void outputRound(Round round, int count) {

		// go through the problems in the round'ψ.lψ
		for (int i = 0; i < round.getProblems().size(); i++) {
			// output the problem details to excel
			writeNumberToCell(problemsSheet, 0, row, count);
			writeStringToCell(problemsSheet, 1, row, round.getProblems().get(i).toString());
			writeStringToCell(problemsSheet, 2, row, round.getProblems().get(i)
					.getTypesString());
			writeStringToCell(problemsSheet, 3, row, round.getProblems().get(i).getDifficultyString());
			writeNumberToCell(problemsSheet, 4, row, round.getProblems()
					.get(i).getReward());
			writeStringToCell(problemsSheet, 5, row, "" + round.getProblems().get(i).isSolved());
			writeStringToCell(problemsSheet, 6, row, "" + round.getProblems().get(i).compositeProblemString());
			row++;
		}

		// go through the agents from the round
		for (int i = 0; i < round.getSolvers().size(); i++) {
			// add agent details to excel
			writeNumberToCell(agentsSheet, 0, t, count);
			writeStringToCell(agentsSheet, 1, t, round.getSolvers().get(i).toString());
			writeStringToCell(agentsSheet, 2, t, round.getSolvers().get(i)
					.getSkillsString());
			writeStringToCell(agentsSheet, 3, t, round.getSolvers().get(i).getExperienceString());
			writeStringToCell(agentsSheet, 4, t, round.getSolvers().get(i).compositeExpString());
			writeNumberToCell(agentsSheet, 5, t, round.getSolvers().get(i)
					.getMoney());
			
			//writeNumberToCell(agentsSheet, 5, t, round.getSolvers().get(i).getExp(0));
			//writeNumberToCell(agentsSheet, 6, t, round.getSolvers().get(i).getExp(1));
			//writeNumberToCell(agentsSheet, 7, t, round.getSolvers().get(i).getExp(2));
			//writeNumberToCell(agentsSheet, 8, t, round.getSolvers().get(i).getExp(3));
			
			if (round.getSolvers().get(i).hasSolved() != null
					&& round.getSolvers().get(i).hasSolved().length() > 4) {
				writeStringToCell(agentsSheet, 6, t, round.getSolvers().get(i).hasSolved());
			} else {
				writeStringToCell(agentsSheet, 6, t, "No problem");
			}
			writeStringToCell(agentsSheet, 7, t, "" + round.getSolvers().get(i).wasInGroup());
			round.getSolvers().get(i).setInGroup(false);
			writeNumberToCell(agentsSheet, 8, t, round.getSolvers().get(i).getTotalRejected());
			t++;
		}
	}

	public void writeStringToCell(WritableSheet sheet, int column, int row, String value) {
		label = new Label(column, row, ""
				+ value);
		try {
			sheet.addCell(label);
		} catch (WriteException e) {
			e.printStackTrace();
		}
	}

	public void writeNumberToCell(WritableSheet sheet, int column, int row, int value) {
		try {
			sheet.addCell(new Number(column, row, value));
		} catch (WriteException e) {
			e.printStackTrace();
		}
	}

	// output averages to excel and graph images
	public void outputAverages() {

		Double[] avgProblemsSolvedperRound = new Double[rounds.size()];
		double avgProblemsSolvedperGame = 0.0;

		// creating a graph object
		/*XYSeries compositeExpSeries = new XYSeries(
				"Average Agent Experience");
		XYSeries compositeExpMSeries = new XYSeries ("Agent experience minus deviation");
		XYSeries compositeExpPSeries = new XYSeries ("Agent experience plus deviation");*/
        YIntervalSeries compositeExpSeries = new YIntervalSeries("Solvers");
		YIntervalSeries compositeProblemSeries = new YIntervalSeries(
				"Proposers");
		
		XYSeries avgProbsRoundsSeries = new XYSeries(
				"Fraction of problems solved");

		XYSeries agentsPerRoundSeries = new XYSeries(
				"Solvers");
		XYSeries proposersPerRoundSeries = new XYSeries(
				"Proposers");

		for (int j = 0; j < agentsPerRound.length; j++) {
			agentsPerRoundSeries.add(j, agentsPerRound[j]);
		}
		
		//adding the data series for the deviation of the agents experience
		for (int j = 0; j < rounds.size() -1; j++) {		
			compositeExpSeries.add(j, solverAvg.get(j), solverDevMAvg.get(j), solverDevPAvg.get(j));
		}
		/*
		for (int j = 0; j < rounds.size(); j++) {		
			compositeExpMSeries.add(j, solverDevMAvg.get(j));
		}
		for (int j = 0; j < rounds.size(); j++) {		
			compositeExpPSeries.add(j, solverDevPAvg.get(j));
		}*/
		for (int j = 0; j < rounds.size() -1; j++) {		
			compositeProblemSeries.add(j, problemAvg.get(j), problemMDevMAvg.get(j), problemPDevPAvg.get(j));
		}

		for (int t = 0; t < proposersPerRound.length; t++) {
			proposersPerRoundSeries.add(t, proposersPerRound[t]);
		}

		// go through rounds
		for (int i = 0; i < rounds.size(); i++) {
			int x = 0;
			// go through problems to find out how many have been solved
			for (int j = 0; j < rounds.get(i).getProblems().size(); j++) {
				if (rounds.get(i).getProblems().get(j).isSolved())
					x++;
			}
/* 
 * 
 * 
 * 
 * 
 * 
*/
			writeStringToCell(averagesSheet, 1, i, "Fraction of problems solved " + (i + 1));

			avgProblemsSolvedperRound[i] = (double) x
					/ rounds.get(i).getProblems().size();

			writeStringToCell(averagesSheet, 2, i, "" + avgProblemsSolvedperRound[i]);
			avgProbsRoundsSeries.add(i + 1, avgProblemsSolvedperRound[i]);
		}

		// create graph object
		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(avgProbsRoundsSeries);
		
        YIntervalSeriesCollection datasetDP = new YIntervalSeriesCollection();
        datasetDP.addSeries(compositeExpSeries);
        datasetDP.addSeries(compositeProblemSeries);
		/*XYSeriesCollection dataset7 = new XYSeriesC
		 * ollection();
		dataset7.addSeries(compositeExpSeries);
		XYSeriesCollection dataset9 = new XYSeriesCollection();
		dataset7.addSeries(compositeExpMSeries);
		XYSeriesCollection dataset10 = new XYSeriesCollection();
		dataset7.addSeries(compositeExpPSeries);*/

		XYSeriesCollection dataset2 = new XYSeriesCollection();
		XYSeriesCollection dataset3 = new XYSeriesCollection();
		dataset2.addSeries(agentsPerRoundSeries);
		dataset3.addSeries(proposersPerRoundSeries);

		// write the averages in the graph
		JFreeChart avgProbRoundsChart = ChartFactory.createXYLineChart(
				"Fraction of problems solved ", "Rounds",
				"Fraction of total problems", dataset);

		JFreeChart agentsPerRoundChart = ChartFactory.createXYLineChart(
				"Number of agents per round", "Rounds", "Number of agents",
				dataset2);

		XYPlot plot = agentsPerRoundChart.getXYPlot();
		plot.setDataset(0, dataset2);
		plot.setDataset(1, dataset3);
		XYLineAndShapeRenderer renderer0 = new XYLineAndShapeRenderer();
		XYLineAndShapeRenderer renderer1 = new XYLineAndShapeRenderer();
		plot.setRenderer(0, renderer0);
		plot.setRenderer(1, renderer1);
		plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(0,
				Color.red);
		plot.getRendererForDataset(plot.getDataset(1)).setSeriesPaint(0,
				Color.blue);

		
		JFreeChart compositeChart = ChartFactory.createXYLineChart(
				"Average skill/difficulty", "Rounds", "Exp/Difficulty",
				datasetDP, PlotOrientation.VERTICAL, true, true, false);
		XYPlot xyplot = (XYPlot)compositeChart.getPlot();
		xyplot.setDomainPannable(true);
		xyplot.setRangePannable(false);
		xyplot.setInsets(new RectangleInsets(5D, 5D, 5D, 20D));
		DeviationRenderer deviationrenderer = new DeviationRenderer(true, false);
		deviationrenderer.setSeriesStroke(0, new BasicStroke(3F, 1, 1));
		deviationrenderer.setSeriesStroke(0, new BasicStroke(3F, 1, 1));
		deviationrenderer.setSeriesStroke(1, new BasicStroke(3F, 1, 1));
		deviationrenderer.setSeriesFillPaint(0, new Color(255, 200, 200));
		deviationrenderer.setSeriesFillPaint(1, new Color(200, 200, 255));
		xyplot.setRenderer(deviationrenderer);
		NumberAxis numberaxis = (NumberAxis)xyplot.getRangeAxis();
		numberaxis.setAutoRangeIncludesZero(false);
		numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		XYPlot xyPlot2 = agentsPerRoundChart.getXYPlot();
		NumberAxis domainAxis2 = (NumberAxis) xyPlot2.getDomainAxis();
		NumberAxis domainAxis21 = (NumberAxis) xyPlot2.getRangeAxis();
		domainAxis21.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		domainAxis2.setRange(1, rounds.size());
		domainAxis2.setTickUnit(new NumberTickUnit(5));
		
		/*XYPlot xyPlot3 = compositeChart.getXYPlot();
		NumberAxis domainAxis3 = (NumberAxis) xyPlot3.getDomainAxis();
		NumberAxis domainAxis31 = (NumberAxis) xyPlot3.getRangeAxis();
		domainAxis31.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		domainAxis3.setRange(1, rounds.size());
		domainAxis3.setTickUnit(new NumberTickUnit(5));*/

		try {
			// create the image file on the desktop
			ChartUtilities.saveChartAsJPEG(new File(path
					+ "\\Desktop\\numberAgents" + data + ".jpg"),
					agentsPerRoundChart, 1400, 700);

			ChartUtilities.saveChartAsJPEG(new File(path
					+ "\\Desktop\\avgProblems" + data + ".jpg"),
					avgProbRoundsChart, 1400, 700);
			
			ChartUtilities.saveChartAsJPEG(new File(path
					+ "\\Desktop\\composite" + data + ".jpg"),
					compositeChart, 1400, 700);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		double total = 0;

		// calculating the average of how many agents have solved in the game
		for (int i = 0; i < avgProblemsSolvedperRound.length; i++) {
			total = total + avgProblemsSolvedperRound[i];
		}

		avgProblemsSolvedperGame = total / avgProblemsSolvedperRound.length;

		// output the average to the excel file
		writeStringToCell(averagesSheet, 5, 0, "Percentage problems solved in game ");
		writeStringToCell(averagesSheet, 6, 0, "" + avgProblemsSolvedperGame);

		// __________________________

		// create graph object
		XYSeries avgAgentsRoundsSeries = new XYSeries(
				"Fraction of successful agents ");

		for (int i = 0; i < avgProblemsSolvedperRound.length; i++) {
			writeStringToCell(averagesSheet, 9, i, "Fraction of successful agents "
					+ (i + 1));
			writeStringToCell(averagesSheet, 10, i, "" + avgAgentsSolvedperRound[i]);
			avgAgentsRoundsSeries.add(i + 1, avgAgentsSolvedperRound[i]);
		}

		// create graph object
		XYSeriesCollection dataset1 = new XYSeriesCollection();
		dataset1.addSeries(avgAgentsRoundsSeries);

		JFreeChart avgAgentsRoundsChart = ChartFactory.createXYLineChart(
				"Fraction of agents having solved ", "Rounds",
				"Fraction of successful agents", dataset1);

		// setting the axises of the graph
		XYPlot xyPlot1 = avgAgentsRoundsChart.getXYPlot();
		NumberAxis domainAxis1 = (NumberAxis) xyPlot1.getDomainAxis();
		domainAxis1.setRange(1, rounds.size());
		domainAxis1.setTickUnit(new NumberTickUnit(5));

		try {
			// save graph as image on desktop
			ChartUtilities.saveChartAsJPEG(new File(path
					+ "\\Desktop\\avgAgents" + data + ".jpg"),
					avgAgentsRoundsChart, 1400, 800);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		total = 0;

		// going through agents to calculate the average for the game
		for (int i = 0; i < avgAgentsSolvedperRound.length; i++) {
			total = total + avgAgentsSolvedperRound[i];
		}

		avgAgentsSolvedperGame = total / avgAgentsSolvedperRound.length;

		writeStringToCell(averagesSheet, 5, 2, "Fraction of successful agents in game ");
		writeStringToCell(averagesSheet, 6, 2, "" + avgAgentsSolvedperGame);
	}
}
